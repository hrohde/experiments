import pyglet

import pymunk
from pyglet.clock import schedule_interval
from pyglet.gl import glClearColor, GL_POINTS, GL_LINES
from pymunk import Vec2d

from playground import COLLTYPE_DEFAULT


class Dropper(pyglet.window.Window):

    def __init__(self):
        super(Dropper, self).__init__(fullscreen=True)

        ### Init pymunk and create space

        self.space = pymunk.Space()
        self.space.gravity = (0.0, -900.0)
        self.space.damping = .999

        self.run_physics = True

        self.balls = [
            self.create_ball(Vec2d(100, 300), 1)
        ]

        self.segments = self.create_static_segments()

        schedule_interval(self.update, 1 / 30.)

    def update(self, dt):
        self.space.step(dt)

    def create_ball(self, pos: Vec2d, mass=1.0, radius=15.0):

        moment = pymunk.moment_for_circle(mass, 0.0, radius)
        ball_body = pymunk.Body(mass, moment)
        ball_body.position = pos

        ball_shape = pymunk.Circle(ball_body, radius)
        ball_shape.friction = 1.5
        ball_shape.collision_type = COLLTYPE_DEFAULT
        self.space.add(ball_body, ball_shape)
        return ball_shape

    def draw_ball(self, ball):
        pyglet.gl.glColor3f(1, 0, 1)
        pyglet.gl.glPointSize(ball.radius * 2)

        pos = (ball.body.position.x, ball.body.position.y)

        pyglet.graphics.draw(
            1, GL_POINTS,
            ('v2f', pos),
            # ('c3B'), (255, 0, 0, 0, 0, 255)
        )

    def create_static_segments(self):
        segments = [
            pymunk.Segment(self.space.static_body, (100., 100.), (1820., 50.), 5)
        ]
        self.space.add(segments)
        return segments

    def draw_static_segments(self, segments):
        pyglet.gl.glColor3f(0., 1., 0.)

        for s in segments:
            pyglet.gl.glLineWidth(s.radius * 2)
            pyglet.graphics.draw(
                2, GL_LINES,
                ('v2f', (s.a.x, s.a.y, s.b.x, s.b.y))
            )

    def on_draw(self):
        glClearColor(0, 0, 0, 255)
        self.clear()

        ### Draw balls
        for ball in self.balls:
            self.draw_ball(ball)

        ## Draw static
        self.draw_static_segments(self.segments)


if __name__ == '__main__':
    Dropper()
    pyglet.app.run()
