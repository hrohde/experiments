from pyglet.gl import *
from pyglet.window import key

import numpy

INCREMENT = 1

DETAIL_LEVEL = 3
MAP_SIZE = 2 ** DETAIL_LEVEL + 1


class TerrainWindow(pyglet.window.Window):
    # Cube 3D start rotation
    xRotation = yRotation = 0
    heightMap = numpy.zeros((MAP_SIZE, MAP_SIZE))

    def random(self, level):
        # level ranges from 0 to DETAIL_LEVEL-1
        # return numpy.random.standard_normal() / 2**level
        return numpy.random.standard_normal() / (level + 1) ** 2
        # return 0

    def subdivide(self, level, x1, y1, x2, y2):
        # calculate center of rectangle
        xc = (x1 + x2) >> 1
        yc = (y1 + y2) >> 1

        # first move the center:
        self.heightMap[xc][yc] = \
            self.random(level) + (
                        self.heightMap[x1, y1] + self.heightMap[x1, y2] + self.heightMap[x2, y1] + self.heightMap[
                    x2, y2]) / 4

        # move center-points of edges:
        # TODO optimization: inner points will be set repeatedly by subdivision of adjacent rectangles
        self.heightMap[xc][y1] = self.random(level) + (self.heightMap[x1, y1] + self.heightMap[x2, y1]) / 2
        self.heightMap[x2][yc] = self.random(level) + (self.heightMap[x2, y1] + self.heightMap[x2, y2]) / 2
        self.heightMap[xc][y2] = self.random(level) + (self.heightMap[x1, y2] + self.heightMap[x2, y2]) / 2
        self.heightMap[x1][yc] = self.random(level) + (self.heightMap[x1, y1] + self.heightMap[x1, y2]) / 2

        if level < (DETAIL_LEVEL - 1):
            # subdivide next level
            self.subdivide(level + 1, x1, y1, xc, yc)
            self.subdivide(level + 1, xc, y1, x2, yc)
            self.subdivide(level + 1, x1, yc, xc, y2)
            self.subdivide(level + 1, xc, yc, x2, y2)

    def createHeightMap(self):

        # start with four corners
        self.heightMap[0][0] += self.random(0)
        self.heightMap[0][MAP_SIZE - 1] += self.random(0)
        self.heightMap[MAP_SIZE - 1][0] += self.random(0)
        self.heightMap[MAP_SIZE - 1][MAP_SIZE - 1] += self.random(0)

        # recursively divide landscape in smaller segments
        self.subdivide(0, 0, 0, MAP_SIZE - 1, MAP_SIZE - 1)

    def __init__(self):
        super(TerrainWindow, self).__init__(fullscreen=True)
        glClearColor(0, 0, 0, 1)
        glShadeModel(GL_FLAT)
        glEnable(GL_DEPTH_TEST)

        self.createHeightMap()

    def on_draw(self):
        # Clear the current GL Window
        self.clear()

        glPolygonMode(GL_FRONT, GL_FILL)
        glPolygonMode(GL_BACK, GL_LINE)

        # define camera position

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(
            0, 1, -3,   # position
            0, 0, 0,    # look at
            0, 1, 0     # up vector
        )

        glRotatef(self.xRotation, 0, 1, 0)

        #

        # glMatrixMode(GL_PROJECTION) before gluPerspective

        # Push Matrix onto stack
        glPushMatrix()

        # TODO: use pyglet.graphics.Batch to precalculate terrain (see opengl.py)
        # center map at origin
        glScalef(2 / (MAP_SIZE - 1), 2 / (MAP_SIZE - 1), 2 / (MAP_SIZE - 1))

        glTranslatef(-(MAP_SIZE - 1) / 2, 0, -(MAP_SIZE - 1) / 2)

        glBegin(GL_TRIANGLES)

        glColor3f(1, 1, 1)

        odd = False

        for x in range(0, MAP_SIZE - 1):
            for y in range(0, MAP_SIZE - 1):

                if odd:
                    glColor3f(1, 1, 0)
                else:
                    glColor3f(0, 1, 1)

                glVertex3f(x, self.heightMap[x][y], y)
                glVertex3f(x, self.heightMap[x][y + 1], y + 1)
                glVertex3f(x + 1, self.heightMap[x + 1][y], y)

                # col = (numpy.random.standard_normal() + 1) / 2
                glColor3f(1, 0, 1)

                glVertex3f(x, self.heightMap[x][y + 1], y + 1)
                glVertex3f(x + 1, self.heightMap[x + 1][y + 1], y + 1)
                glVertex3f(x + 1, self.heightMap[x + 1][y], y)

                odd = not odd

        glEnd()

        # Pop Matrix off stack
        glPopMatrix()

        # Push Matrix onto stack
        glPushMatrix()

        glBegin(GL_QUADS)

        glColor3f(0, 0, 1)

        glVertex3f(-1, 0, -1)
        glVertex3f(-1, 0, 1)
        glVertex3f(1, 0, 1)
        glVertex3f(1, 0, -1)

        glEnd()

        glBegin(GL_LINES)

        glVertex3f(-100, 0, 0)
        glVertex3f(100, 0, 0)

        glVertex3f(0, -100, 0)
        glVertex3f(0, 100, 0)

        glVertex3f(0, 0, -100)
        glVertex3f(0, 0, 100)

        glEnd()

        # Pop Matrix off stack
        glPopMatrix()

    def on_resize(self, width, height):
        # set the Viewport
        glViewport(0, 0, width, height)

        # using Projection mode
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        aspectRatio = width / height
        gluPerspective(35, aspectRatio, 1, 1000)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glTranslatef(0, -1, -5)

    def on_text_motion(self, motion):
        if motion == key.UP:
            self.yRotation -= INCREMENT
        elif motion == key.DOWN:
            self.yRotation += INCREMENT
        elif motion == key.LEFT:
            self.xRotation -= INCREMENT
        elif motion == key.RIGHT:
            self.xRotation += INCREMENT


if __name__ == '__main__':
    TerrainWindow()
    pyglet.app.run()
