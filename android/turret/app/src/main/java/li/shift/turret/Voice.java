package li.shift.turret;

import android.content.Context;
import android.media.MediaPlayer;

import org.androidannotations.annotations.EBean;

import java.util.Random;

@EBean(scope = EBean.Scope.Singleton)
public class Voice {

    private static String TAG = "Voice";

    private Context context;
    private MediaPlayer mediaPlayer = null;

    public Voice(Context context) {
        this.context = context;
    }

    private static int[] activateSounds = new int[] {
            R.raw.turret_active_gotcha,
            R.raw.turret_active_hello_friend,
            R.raw.turret_active_hi,
            R.raw.turret_active_i_see_you,
            R.raw.turret_active_there_you_are,
            R.raw.turret_deploy_activated,
            R.raw.turret_deploy_hello,
            R.raw.turret_deploy_there_you_are,
            R.raw.turret_deploy_whos_there,
    };

    private static int[] searchSounds = new int[] {
            R.raw.turret_autosearch_hello,
            R.raw.turret_autosearch_is_anyone_there,
            R.raw.turret_autosearch_searching,
            R.raw.turret_autosearch_sentry_mode_activated,
            R.raw.turret_autosearch_would_you_come_over_here,
            R.raw.turret_search_are_you_still_there,
            R.raw.turret_search_can_i_help_you,
            R.raw.turret_search_searching,
    };
    
    private static int[] retireSounds = new int[] {
            R.raw.turret_retire_goodbye,
            R.raw.turret_retire_goodnight,
            R.raw.turret_retire_hibernating,
            R.raw.turret_retire_naptime,
            R.raw.turret_retire_resting,
            R.raw.turret_retire_sleep_mode_activated,
            R.raw.turret_retire_your_business_is_appreciated,
    };

    private static int[] chatSounds = new int[] {
            R.raw.turret_active_i_see_you,
            R.raw.turret_deploy_hello,
            R.raw.turret_deploy_whos_there,
            R.raw.turret_autosearch_hello,
            R.raw.turret_autosearch_would_you_come_over_here,
            R.raw.turret_search_can_i_help_you,
            R.raw.turret_collide_excuse_me,
            R.raw.turret_pickup_heey,
            R.raw.turret_pickup_hey,
            R.raw.turret_pickup_who_are_you,
            R.raw.turret_disabled_i_dont_hate_you,
    };

    private Random random = new Random();

    public enum Topic {
        Activate,
        Search,
        Retire,
        Chat,
    }

    public void saySomething(Topic topic) {
        int sound = chooseSound(topic);
        play(sound);
    }

    private int chooseSound(Topic topic) {
        int[] sounds = null;
        switch (topic) {
            case Activate:
                sounds = activateSounds;
                break;
            case Search:
                sounds = searchSounds;
                break;
            case Retire:
                sounds = retireSounds;
                break;
            case Chat:
                sounds = chatSounds;
                break;
        }

        if (sounds != null) {
            return sounds[random.nextInt(Integer.MAX_VALUE) % sounds.length];
        } else {
            return 0;
        }
    }

    private void play(int sound) {
        if (mediaPlayer != null) {
            return;
        }

        mediaPlayer = MediaPlayer.create(context, sound);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.release();
                mediaPlayer = null;
            }
        });

        mediaPlayer.start();
    }

}
