package li.shift.turret;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.util.Random;

import li.shift.turret.Voice.Topic;

public class FaceTracker extends Tracker<Face> {

    private static final String TAG = FaceTracker.class.getSimpleName();
    private static final Random random = new Random();

    private GraphicOverlay mOverlay;
    private FaceGraphic mFaceGraphic;
    private Voice voice;

    public FaceTracker(Context context, GraphicOverlay overlay, Voice voice) {
        mOverlay = overlay;
        mFaceGraphic = new FaceGraphic(overlay);
        this.voice = voice;
    }

    /**
     * Start tracking the detected face instance within the face overlay.
     */
    @Override
    public void onNewItem(int faceId, Face item) {
        Log.i(TAG, "onNewItem");
        mFaceGraphic.setId(faceId);
        voice.saySomething(Topic.Activate);
    }

    /**
     * Update the position/characteristics of the face within the overlay.
     */
    @Override
    public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
        mOverlay.add(mFaceGraphic);
        mFaceGraphic.updateFace(face);
        if (random.nextInt(75) == 0) {
            voice.saySomething(Topic.Chat);
        }
    }

    /**
     * Hide the graphic when the corresponding face was not detected.  This can happen for
     * intermediate frames temporarily (e.g., if the face was momentarily blocked from
     * view).
     */
    @Override
    public void onMissing(FaceDetector.Detections<Face> detectionResults) {
        Log.i(TAG, "onMissing");
//        mOverlay.remove(mFaceGraphic);
    }

    /**
     * Called when the face is assumed to be gone for good. Remove the graphic annotation from
     * the overlay.
     */
    @Override
    public void onDone() {
        Log.i(TAG, "onDone");
        mOverlay.remove(mFaceGraphic);
        voice.saySomething(Topic.Retire);
    }

    public static class Factory implements MultiProcessor.Factory<Face> {

        private final GraphicOverlay graphicOverlay;
        private final Context context;
        private final Voice voice;

        public Factory(Context context, GraphicOverlay graphicOverlay, Voice voice) {
            this.graphicOverlay = graphicOverlay;
            this.context = context;
            this.voice = voice;
        }

        @Override
        public Tracker<Face> create(Face face) {
            return new FaceTracker(context, graphicOverlay, voice);
        }
    }

}
