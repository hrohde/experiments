package li.shift.turret;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.face.FaceDetector;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;

@EActivity(R.layout.activity_turret)
public class TurretActivity extends AppCompatActivity {

    private static final String TAG = TurretActivity.class.getSimpleName();

    private FaceDetector faceDetector;
    private CameraSource cameraSource;

    @Bean
    Voice voice;

    @ViewById(R.id.camera_source_preview)
    CameraSourcePreview cameraSourcePreview;

    @ViewById(R.id.graphic_overlay)
    GraphicOverlay graphicOverlay;

    @Override
    protected void onStart() {
        super.onStart();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        faceDetector = new FaceDetector.Builder(this)
                .setTrackingEnabled(true)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .setMode(FaceDetector.FAST_MODE)
                .build();

        faceDetector.setProcessor(
                new MultiProcessor.Builder<>(new FaceTracker.Factory(getApplicationContext(), graphicOverlay, voice)).build()
        );

        DisplayMetrics m = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(m);

        cameraSource = new CameraSource.Builder(getApplicationContext(), faceDetector)
                .setRequestedPreviewSize(m.widthPixels, m.heightPixels)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .build();
    }

    @Override
    protected void onStop() {
        super.onStop();
        cameraSource.release();
        faceDetector.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            cameraSourcePreview.start(cameraSource);
        } catch (IOException e) {
            Log.e(TAG, "openCamera failed", e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraSourcePreview.stop();
    }

}
