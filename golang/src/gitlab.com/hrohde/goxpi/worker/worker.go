package worker

import "fmt"

type Worker struct {
	Source chan interface{}
	quit   chan struct{}
}

func (w *Worker) Start() {
	w.Source = make(chan interface{}, 10) // some buffer size to avoid blocking
	go func() {
		for {
			select {
			case msg := <-w.Source:
				// do something with msg
				fmt.Println("worker does something")
				fmt.Println(msg)

			case <-w.quit: // will explain this in the last section
				return
			}
		}
	}()
}

