package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"gitlab.com/hrohde/goxpi/worker"
	"time"
)

func main() {

	// channel to receive OS signals
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	// channel to indicate process termination
	terminated := make(chan struct{})

	go func() {
		sig := <-signals
		fmt.Println("Received " + sig.String() + " - terminating")
		close(terminated)
	}()

	workers := []*worker.Worker{&worker.Worker{}, &worker.Worker{}}
	for _, w := range workers { w.Start() }

	// set up ticker
	ticker := time.NewTicker(1000 * time.Millisecond)

	go func() {
		for t := range ticker.C {
			fmt.Println("sending tick", t)
			msg := t.Unix()
			for _, w := range workers {
				w.Source <- msg
			}
		}
	}()

	// wait for termination
	<-terminated

	println("shutting down...")

	ticker.Stop()

}
